PIXEL2016_CODENAMES += \
    %marlin \
    %sailfish

PIXEL2017_CODENAMES += \
    %muskie \
    %taimen \
    %wahoo \
    %walleye

PIXEL2018_CODENAMES += \
    %blueline \
    %crosshatch

PIXEL2019_MIDYEAR_CODENAMES += \
    %bonito \
    %sargo

PIXEL2019_CODENAMES += \
    %coral \
    %flame

PIXEL2020_CODENAMES += \
    %barbet \
    %bramble \
    %redfin

PIXEL2021_CODENAMES += \
    %oriole \
    %raven

PIXEL2021_MIDYEAR_CODENAMES += \
    %bluejay

PIXEL2022_CODENAMES += \
    %panther \
    %cheetah

PIXEL2022_MIDYEAR_CODENAMES += \
    %lynx

PIXELTABLET_CODENAMES += \
    %tangorpro \

PIXEL_CODENAMES += \
    $(PIXEL2016_CODENAMES) \
    $(PIXEL2017_CODENAMES) \
    $(PIXEL2018_CODENAMES) \
    $(PIXEL2019_MIDYEAR_CODENAMES) \
    $(PIXEL2019_CODENAMES) \
    $(PIXEL2020_CODENAMES) \
    $(PIXEL2021_CODENAMES) \
    $(PIXEL2021_MIDYEAR_CODENAMES) \
    $(PIXEL2022_CODENAMES) \
    $(PIXEL2022_MIDYEAR_CODENAMES) \
    $(PIXELTABLET_CODENAMES)
