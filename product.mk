include vendor/google_pixel/codenames.mk

## Pixel Apps
include vendor/google_pixel/apps/GoogleApps.mk

## Pixel Sounds
# Don't build on devices with limited partition sizes
#ifneq ($(GMS_MAKEFILE),gms_minimal.mk)
include vendor/google_pixel/sounds/GoogleAudio.mk
#endif

## Pixel Theme
PRODUCT_PACKAGES += \
    PixelSetupWizardStringsOverlay

PRODUCT_PRODUCT_PROPERTIES += ro.atrace.core.services=com.google.android.gms,com.google.android.gms.ui,com.google.android.gms.persistent
PRODUCT_SYSTEM_DEFAULT_PROPERTIES += \
    ro.boot.vendor.overlay.theme=com.android.internal.systemui.navbar.gestural;com.android.theme.icon.circle;org.lineageos.overlay.customization.navbar.nohint \
    ro.com.google.ime.bs_theme=true \
    ro.com.google.ime.theme_id=5 \
    ro.opa.eligible_device=true \
    ro.setupwizard.enterprise_mode=1 \
    ro.setupwizard.esim_cid_ignore=00000001 \
    setupwizard.feature.baseline_setupwizard_enabled=true \
    setupwizard.feature.show_pai_screen_in_main_flow.carrier1839=false \
    setupwizard.feature.show_pixel_tos=true \
    setupwizard.feature.skip_button_use_mobile_data.carrier1839=true

# Artifact Path Exclusions
PRODUCT_ARTIFACT_PATH_REQUIREMENT_ALLOWED_LIST += \
    system/app/GoogleExtShared/GoogleExtShared.apk \
    system/etc/permissions/privapp-permissions-pixel-s.xml \
    system/etc/permissions/privapp-permissions-pixel.xml \
    system/etc/permissions/privapp_allowlist_com.google.android.ext.services.xml \
    system/priv-app/GoogleExtServicesMobile/GoogleExtServicesMobile.apk

# Disable remote keyguard animation
# Only here due to Pixel Launcher bug
PRODUCT_SYSTEM_DEFAULT_PROPERTIES += \
    persist.wm.enable_remote_keyguard_animation=0

## Sepolicy
BOARD_SEPOLICY_DIRS += \
    hardware/google/pixel-sepolicy/flipendo
